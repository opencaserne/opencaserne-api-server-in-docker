FROM node:alpine

MAINTAINER Valentin LORTET <valentin@lortet.io>

WORKDIR /usr/src/app/
COPY node/ /usr/src/app/
RUN npm ci --no-optionnal && npm cache clean --force
CMD node .
